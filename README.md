Build a version-controlled key-value store with an HTTP API we can query that from.

The API needs to be able to:
1. Accept a key(string) and value(some JSON blob/string) and store them. If an
existing key is sent, the value should be updated
2. Accept a key and return the corresponding latest value
3. When given a key AND a timestamp, return whatever the value of the key at the
time was.
4. Displays all values currently stored in the database.
Assume only GET and POST requests for simplicity.
All timestamps are UNIX timestamps according to the UTC timezone.

Example:

Method: POST
Endpoint: /object
Body: JSON: {mykey : value1}
Time: 6pm

Method: GET
Endpoint: /object/mykey
Response: value1 object

Method: POST
Endpoint: /object
Body: JSON: {mykey : value2}
Time: 6.05 pm

Method: GET
Endpoint: /object/mykey
Response: value2 object

Method: GET
Endpoint: /object/mykey?timestamp=1440568980 [6.03pm]
Response: value1 object

Method: GET
Endpoint: /object/get_all_records
Response: Returns JSON Array of all records data and their values currently stored in the DB
