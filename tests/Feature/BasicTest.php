<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BasicTest extends TestCase
{
    public function testAPIPostOK()
    {
        $response = $this->postJson('/object', ['key1' => 'value1']);
        $response->assertStatus(201);
    }
    
    public function testAPIGetOK()
    {
        $response = $this->get('/object/key1');
        $response->assertSee($response['key1']);
    }
    
    public function testAPIPostOK2()
    {
        sleep(2); // sleep for 2 seconds
        $response = $this->postJson('/object', ['key1' => 'value2']);
        $response->assertStatus(201);
    }   
    
    public function testAPIGetTimestamp()
    {
        $just_now = time() - 1;
        $response = $this->get('/object/key1?timestamp=' . $just_now);
        $response->assertJson(['key1' => 'value1']);
    }   
    
    public function testAPIGetAllRecords()
    {
        $response = $this->get('/object/get_all_records');
        $response->assertSee($response['key1']);    
    }
    
    public function testAPIGetInvalidPath()
    {
       $response = $this->get('/subject/key1');
       $response->assertStatus(404);    
    }

    public function testAPIGetKeyNotFound()
    {
        $response = $this->get('/object/nonexistentkey');
        $response->assertStatus(404)->assertJson(['success' => 0]);
    }
 
    public function testAPIGetMissingTimestamp()
    {
        $response = $this->get('/object/key1?datetime=' . time());
        $response->assertStatus(400)->assertJson(['success' => 0]);
    }
 
    public function testAPIGetTimestampNotInteger()
    {
        $response = $this->get('/object/key1?timestamp=FourthOfNov2021');
        $response->assertStatus(400)->assertJson(['success' => 0]);
    } 

    public function testAPIGetTimestampNegative()
    {
        $response = $this->get('/object/key1?timestamp=-14182980');
        $response->assertStatus(400)->assertJson(['success' => 0]);
    } 
    
    public function testAPIGetTimestampObjectNotCreated()
    {
        $long_ago = strtotime('1990-01-01 09:00:00');
        $response = $this->get('/object/key1?timestamp=' . $long_ago);
        $response->assertStatus(404)->assertJson(['success' => 0]);
    }   

    public function testAPIGetTooManyParameters()
    {
        $response = $this->get('/object/key1?name=john&age=35');
        $response->assertStatus(400)->assertJson(['success' => 0]);
    } 

    public function testAPIPostInvalidPath()
    {
       $response = $this->postJson('/subject', ['key1' => 'value1']);
       $response->assertStatus(404);    
    }
    
    public function testAPIPostEmptyData()
    {
       $response = $this->post('/object');
       $response->assertStatus(200)->assertJson(['success' => 0]);  
    }   
    
    public function testAPIPostInvalidJSON()
    {
       $response = $this->post('/object', ['key1' => 'value1']);
       $response->assertStatus(200)->assertJson(['success' => 0]);  
    }       

    public function testAPIPostIncompleteJSONKey()
    {
       $response = $this->postJson('/object', ['' => '']);
       $response->assertStatus(200)->assertJson(['success' => 0]);  
    }   
    
    public function testAPIPostIncompleteJSONValue()
    {
       $response = $this->postJson('/object', ['key1' => '']);
       $response->assertStatus(200)->assertJson(['success' => 0]);  
    }
    
    public function testAPIPostTooManyJSONKeys()
    {
        $response = $this->postJson('/object', ['key1' => 'value1', 'key2' => 'value2', 'key3' => 'value3']);
        $response->assertStatus(200)->assertJson(['success' => 0]); 
    }
        

}
