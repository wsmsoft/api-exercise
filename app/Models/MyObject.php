<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MyObject extends Model
{
    use HasFactory;
    protected $table = 'objects';
    //public $timestamps = false;
    protected $primaryKey = 'object_id';	
    protected $fillable = ['object_key'];	
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'updated_on';
	
    public function myObjectHistory()
    {
       return $this->hasMany(MyObjectHistory::class, 'object_id');
    }	
}
