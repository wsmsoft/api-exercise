<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MyObject; 
use App\Models\MyObjectHistory; 
use Carbon\Carbon; 

class ObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$data = array();
	foreach (MyObject::all() as $obj) {
	   $objHistory = $obj->myObjectHistory()->orderBy('created_on','DESC')->first();
	   $data[$obj->object_key] = $objHistory->object_value;
	}
	return response()->json($data, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty($request->getContent()))
            return response()->json(['success' => 0, 'error_desc'=>'no data'], 200);                
           
        $data = json_decode($request->getContent(), true);
        if(empty($data))
            return response()->json(['success' => 0, 'error_desc'=>'invalid JSON data'], 200);      

        if(count($data)>1)
            return response()->json(['success' => 0, 'error_desc'=>'too many JSON parameters'], 200);
        
        $first_key = key($data);
        $first_val = $data[$first_key];
        
        if(empty($first_key))
            return response()->json(['success' => 0, 'error_desc'=>'missing key'], 200);                
        if(empty($first_val))
            return response()->json(['success' => 0, 'error_desc'=>'missing value'], 200);              
        
        $obj = MyObject::firstOrCreate([
            'object_key' => $first_key
        ]);
        
        $objHistory = new MyObjectHistory;
        $objHistory->object_value = $first_val;     
        $post = $obj->myObjectHistory()->save($objHistory);
    
        return response()->json(['success' => 1, $first_key=>$first_val], 201);          
     }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $timestamp_query = '';
        $query_param_count = count($request->query());
        
        if($query_param_count==1){
            if( $request->has('timestamp')) {
                $timestamp_query = $request->query('timestamp');                
                if(!is_numeric($timestamp_query))
                    return response()->json(['success' => 0, 'error_desc'=>'timestamp not a number'], 400);
                else {
                    $timestamp_val = intval($timestamp_query);  
                    if($timestamp_val<0)
                        return response()->json(['success' => 0, 'error_desc'=>'timestamp out of range'], 400);
                }
            }   
            else 
                return response()->json(['success' => 0, 'error_desc'=>'missing timestamp parameter'], 400);
        }
        else if($query_param_count>1)
            return response()->json(['success' => 0, 'error_desc'=>'too many parameters'], 400);
        
        $obj = MyObject::where(['object_key'=>$id])->first();       
        if(is_null($obj)) 
            return response()->json(['success' => 0, 'error_desc'=>'value not found'], 404);
        
        if(empty($timestamp_query))
            $objHistory = $obj->myObjectHistory()->orderBy('created_on','DESC')->first();
        else{
            $objHistory = $obj->myObjectHistory()->where('created_on', '<', date('Y-m-d H:i:s', $timestamp_query))->orderBy('created_on', 'DESC')->first();
            if(is_null($objHistory))
                return response()->json(['success' => 0, 'error_desc'=>'value not found at timestamp'], 404);
        }   
        return response()->json([$id =>$objHistory->object_value], 200);        
    }   

    public function default()
    {
        return response()->json(['success' => 0, 'error_desc'=>'resource not found'], 404);
    }

}
