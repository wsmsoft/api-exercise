<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('object', 'App\Http\Controllers\ObjectController@index');
Route::get('object/get_all_records', 'App\Http\Controllers\ObjectController@index');
Route::get('object/{id}', 'App\Http\Controllers\ObjectController@show');
Route::post('object', 'App\Http\Controllers\ObjectController@store');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::any('/{any}', 'App\Http\Controllers\ObjectController@default')->where('any', '.*');
